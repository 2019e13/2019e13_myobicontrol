import grips
import dataAccessController as dataAC

def handGrip():
    if dataAC.checkFilteredReady():
        data = dataAC.fetchFilteredData()
        sortedGrip = dataAC.dataSorting(data)
        grips.selectGrip(sortedGrip)
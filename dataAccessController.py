import handConnect as hc
import pcCommunication as pcC
import modelReader as mR
import modelWriter as mW
import stateDataReader as sDR
import filterDataInterface as fDI



def fetchRawData():
    return fDI.getRawData

def checkRawReady():
    return fDI.rawDataReady()

def saveFilteredData(data):
    fDI.setFilteredData(data)

def checkFilteredReady():
    return sDR.getFilteredReady()

def fetchFilteredData():
    return sDR.getFilteredData()

def sendToPc(dataString):
    pcC.sendData(dataString)

def receiveFromPc():
    return pcC.receiveData()

def saveModel(model):
    mW.storeModel(model)

def dataSorting(data):
    return mR.sortData(data)

def commandHand(command):
    hc.sendCommand(command)
import dataStorage as dS

def setRawData(data):
    for i in reversed(data):
        data[i] = data[i+1]
    dS.rawData[0] = data
    dS.rawReady = True
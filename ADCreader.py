import ADCdataWriter as dW
from smbus2 import SMBus
import gpiozero
import time as t

conversionAdr = 0x00
channelSelectAdr = 0x01
channelSelectSetup = 0x07
configAdr = 0x02
configSetupH = 0x08
configSetupL = 0x38
configSetup = [configSetupH, configSetupL]
alertAdr = 0x03
ADCadrHH = 0x20
ADCadrHNC = 0x22
ADCadrHL = 0x23
ADCadrNCH = 0x28
ADCadrNCNC = 0x2A
ADCadrNCL = 0x2B
ADCadrLH = 0x2C
ADCadrLNC = 0x2E
ADCadrLL = 0x2F
device_bus = 1
temporaryData = [0,0,0,0,0,0]
ADCpin1 = gpiozero.OutputDevice(17,True,True,None)
ADCpin2 = gpiozero.OutputDevice(27,True,True,None)
busyPin = gpiozero.InputDevice(22,False,None,None)
resetPin = gpiozero.OutputDevice(23,True,False,None)

def ADCsetup():
    setupSingle(ADCadrLL)
    setupSingle(ADCadrLH)

def readData():
    startConversion()
    readAndCalculate()
    startConversion()
    readAndCalculate()
    startConversion()
    readAndCalculate()

def setupSingle(adc):
    with SMBus(device_bus) as bus:
        bus.write_byte_data(adc,channelSelectAdr,channelSelectSetup)
        bus.write_byte_data(adc,configAdr,configSetup)

def startConversion():
    ADCpin1.off()
    ADCpin2.off()
    t.sleep(0.0000025)
    ADCpin1.on()
    ADCpin2.on()
    t.sleep(0.000001)

def readAndCalculate():
    global temporaryData
    with SMBus(device_bus) as bus:
        data1 = bus.read_i2c_block_data(ADCadrHH,conversionAdr,2)
        data2 = bus.read_i2c_block_data(ADCadrHL,conversionAdr,2)
    
    dat1 = data1[0] >> 5
    dat2 = data2[0] >> 5
    data11 = (data1[0] << 8) + data1[1]
    data11 = data11 & 0b0000111111111111
    data21 = (data2[0] << 8) + data2[1]
    data21 = data21 & 0b0000111111111111

    if dat1 == 0b00:
        temporaryData[0] = data11
    elif dat1 == 0b01:
        temporaryData[1] = data11
    elif dat1 == 0b10:
        temporaryData[2] = data11
    else:
        dat1 = dat1
    
    if dat2 == 0b00:
        temporaryData[3] = data21
    elif dat2 == 0b01:
        temporaryData[4] = data21
    elif dat2 == 0b10:
        temporaryData[5] = data21
    else:
        dat2 = dat2 

ADCsetup()

while 1:
    readData()
    dW.setRawData(temporaryData)
import time as t
import dataAccessController as dataAC


def selectGrip(sig):
    if sig == 'a':
        openHand()
    elif sig == 'b':
        closeHand()
    elif sig == 'c':
        pinch1()
    elif sig == 'd':
        pinch2()
    elif sig == 'e':
        bagGrab()
    elif sig == 'f':
        bottleGrab()


def openHand():
    #print("openHand")
    dataAC.commandHand(b'G0 O \n\r F0 P0 \n\r')

def closeHand():
    dataAC.commandHand(b'G0 C \n\r')

def pinch1():
    dataAC.commandHand(b'F0 P75 \n\r F1 P0 \n\r F2 P0 \n\r F3 P0 \n\r')
    t.sleep(2)
    dataAC.commandHand(b'F1 80 \n\r')

def pinch2():
    dataAC.commandHand(b'F0 P75 \n\r F1 P0 \n\r F2 P0 \n\r F3 P0 \n\r')
    t.sleep(2)
    dataAC.commandHand(b'F1 P80 \n\r F2 P80 \n\r')

def bagGrab():
    dataAC.commandHand(b'G0 C \n\r F0 P0 \n\r')

def bottleGrab():
    dataAC.commandHand(b'F0 P80 \n\r F1 P0 \n\r F2 P0 \n\r F3 P0 \n\r')
    t.sleep(2)
    dataAC.commandHand(b'F1 P80 \n\r F2 P80 \n\r F3 P80 \n\r')

#def fingerPosition(f,p):
#    ff = chr(ord(f) - 1)
#    s = 'F' + ff + ' P' + p + ' \n\r'
#    q = bytes(s, encoding='ascii')
#    return q
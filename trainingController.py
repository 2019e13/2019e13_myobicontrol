import dataAccessController as dataAC
import datetime as dt

def dataTransfer():
    if dataAC.checkFilteredReady == True:
        data = dataAC.fetchFilteredData()
    moment = dt.time()
    uSec = moment.microsecond
    sec = moment.second
    minute = moment.minute
    hour = moment.hour

    dataString = hour + '.' + minute + '.' + sec + '.' + uSec + ';'

    for i in data:
        datastring = datastring + data[i] + ';'
    
    dataAC.sendToPc(dataString)

def modelReceival():
    dataAC.saveModel(dataAC.receiveFromPc())
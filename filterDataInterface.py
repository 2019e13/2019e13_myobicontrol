import dataStorage as dS


def rawDataReady():
    return dS.rawReady

def getRawData():
    dS.rawReady = False
    return dS.rawData

def setFilteredData(data):
    dS.filteredData = data
    dS.filteredReady = True
import trainingController as tc
import operationController as oc

trainingState = False
operationState = True
errorState = False
prevState = 0

def setTrainingState():
    trainingState = True
    operationState = False
    errorState = False
    prevState = 1
    return trainingState, operationState, errorState, prevState

def setOperationState():
    trainingState = False
    operationState = True
    errorState = False
    prevState = 0
    return trainingState, operationState, errorState, prevState

def setErrorState():
    trainingState = False
    operationState = False
    errorState = True
    return trainingState, operationState, errorState

def getState():
    if trainingState == True:
        return 1
    elif operationState == True:
        return 2
    elif trainingState == True:
        return 3
    else:
        return 4

while 1:
    if trainingState == True:
        tc.dataTransfer()
    elif operationState == True:
        oc.handGrip()
    elif errorState == True:
        pass
        #trainingState, operationState, errorState, prevState = setOperationState()
    else:
        trainingState, operationState, errorState, prevState = setOperationState()
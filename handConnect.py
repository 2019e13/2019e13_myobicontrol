import serial

def sendCommand(command):
    with serial.Serial('/dev/serial/by-path/platform-3f980000.usb-usb-0:1.3:1.0', 115200, timeout=1) as ser:
        ser.write(command)

#while 1:
#    i = getInput()
#    w = selectGrip(i)
#    sendCommand(w)
import businessLogicController as bLC
import gpiozero

trainingLED = gpiozero.LED(5,True,False,None)
operationLED = gpiozero.LED(6,True,False,None)
errorLED = gpiozero.LED(13,True,False,None)
fixButton = gpiozero.Button(12,True,True,None,1,False,None)

def indication():
    state = bLC.checkState()

    if state == 1:
        trainingLED.on()
        operationLED.off()
        errorLED.off()
    elif state == 2:
        trainingLED.off()
        operationLED.on()
        errorLED.off()
    elif state == 3:
        trainingLED.off()
        operationLED.off()
        errorLED.on()
        fixButton.wait_for_active(None)
        bLC.errorFixed()

while 1:
    indication()
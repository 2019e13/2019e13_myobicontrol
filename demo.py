import grips

while 1:
    i = input()
    #print(i)
    if i == 'open' or i == 'Open' or i == 'o' or i == 'O':
        grips.selectGrip('a')
    elif i == 'close' or i == 'Close' or i == 'c' or i == 'C':
        grips.selectGrip('b')
    elif i == 'p1' or i == 'p1' or i == 'pinch1' or i == 'pinch1':
        grips.selectGrip('c')
    elif i == 'p2' or i == 'p2' or i == 'pinch2' or i == 'pinch2':
        grips.selectGrip('d')
    elif i == 'bag' or i == 'Bag':
        grips.selectGrip('e')
    elif i == 'bot' or i == 'Bot' or i == 'bottle' or i == 'Bottle':
        grips.selectGrip('f')
import mainStateController as mSC

def checkState():
    return mSC.getState()

def errorFixed():
    if mSC.prevState == 0:
        mSC.trainingState, mSC.operationState, mSC.errorState, mSC.prevState = mSC.setOperationState()
    elif mSC.prevState == 1:
        mSC.trainingState, mSC.operationState, mSC.errorState, mSC.prevState = mSC.setTrainingState()